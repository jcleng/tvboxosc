- 构建

```shell
# 包名
# applicationId 'com.github.tvbox.osc.debug'

# 入口
# app\src\main\java\com\github\tvbox\osc\ui\activity\HomeActivity.java

# 安装debug包
./gradlew installDebug
# 生成release包
./gradlew assemblerelease --build-cache --parallel --daemon --warning-mode all
# 清除依赖缓存
./gradlew assemblerelease --refresh-dependencies --build-cache --parallel --daemon --warning-mode all
```

- 日志

```shell
# 需要打印时间和级别是Error的信息
adb logcat -v time *:E
# 过滤,windows用findstr,linux用grep
adb logcat -v time *:E | grep "TVBox"
```

- android-sdk路径配置文件 `local.properties`

```
sdk.dir=D:\\a\\cmdline-tools\\sdk
```

- 不编译修改包名等信息

```shell
https://github.com/iBotPeaches/Apktool
# 解开包,注意jdk11版本和编译源码的版本一致
D:\work\zulu11.56.19-ca-jdk11.0.15-win_x64\bin\java -jar .\apktool_2.7.0.jar d .\tv\app-release.apk
# 修改D:\a\app-release\AndroidManifest.xml 的包名 package="com.github.tvbox.osc"文件之后打包
D:\work\zulu11.56.19-ca-jdk11.0.15-win_x64\bin\java -jar .\apktool_2.7.0.jar b D:\a\app-release
# I: Built apk into: D:\a\app-release\dist\app-release.apk

# 需要重新签名,最后的参数是别名(TVBoxOSC), 以及修改文件内provider标签的包名
D:\work\zulu11.56.19-ca-jdk11.0.15-win_x64\bin\jarsigner.exe -verbose -keystore D:\work\TVBoxOS\app\TVBoxOSC.jks -signedjar app-release_signed.apk D:\a\app-release\dist\app-release.apk TVBoxOSC
## 输入 TVBoxOSC 密码
## 生成 D:\a\app-release_signed.apk
```

- ndk架构类型,暂不支持请使用兼容程序

```shell
sed -i "s/^.*abiFilters.*$/abiFilters 'armeabi', 'armeabi-v7a', 'arm64-v8a', 'x86', 'x86_64'/" app/build.gradle
```
